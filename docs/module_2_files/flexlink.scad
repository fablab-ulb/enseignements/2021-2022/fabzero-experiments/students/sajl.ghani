/*
    FILE    : flexlink.scad

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-02-21

*/

//parametres
$fn=100;

width=15.8;//mm représente la longueur du bout ovale
depth=7; //mm  représente la largeur du bout ovale
height=3.2;//mm représente la hauteur du bout ovale
length_br=50;//mm la longueur de la branche reliant les 2 extrémités

hole_r=2.4; // le rayon du trou
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous

difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);

}//Création de l'extrémité avec les trous



translate([width,depth/2,0])cube([length_br, depth/8,height]);//création de la tige de connexion

translate([width+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};//création de la deuxième extrémité