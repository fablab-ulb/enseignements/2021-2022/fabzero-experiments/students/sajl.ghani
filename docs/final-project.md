# Final Project

![](images/Alessandro_Abbracciante_Sajl_Ghani.PNG)

Pour l'élaboration de mon projet final, il m'était important de remplir et préciser les 3 étapes du Challenge Learning Framework:

* Engage

* Investigate

* Act

Après les 6 semaines de formations aux outils du fablab, Denis a utilisé le jour d'après pour parler du projet final. Après avoir présenter le Challenge Learning Framework et l'illustrer en présentant son fog net, il a demandé à la classe de partager les différents projets qu'on a en tête pour qu'il nous pousse à avoir une idée large et générale et trouver des synergies entre nous pour un travail de groupe de 2.

Ce qui m'a le plus interesser dans ce cours basé sur les sciences frugal est l'élaboration de version bon marché et ingénieuse d'appareil scientifique côuteuse comme le microscope fait en papier ou la centrifugeuse fait avec des fils ou des cordes et je voudrais que mon projet se porte sur ça. Comment obtenir un produit de manière à payer le moins possible pour un produit chère dans le commerce.

## Engage

Dans cette étape, je dois trouver un challenge qui me motive. Je dois commencer par définir un thème large.

**Big idea**: mon thème est d'obtenir une version bon marché d'un produit cher dans le commerce.

Les questions essentielles que je me pose sont:

* Dans quelle secteur est-ce que je peux trouver des produits qui nécessite que je m'investis?

* Pourquoi est-ce que ce produit est-il chère?

* Quelles sont les outils et matériaux que j'aurais besoin?

Donc mon challenge, mon appel à l'action, est :

* Comment obtenir soi-même un produit onéreux de manière abordable, efficace avec comme contraintes d'utiliser le fablab et des produits bon marché.

## Investigate

### Les questions prioritaires : questions nécessaires à répondre pour gérer le challenge  

* Quels secteurs ont un lien au 17 objectives de développement durable?

* Quel matériaux aurais-je besoin?

* Quel est le prix des matériaux?

* Quelle plan d'action dois-je concevoir?

## Choix du produit

La première chose à faire est de trouver des pistes de produits onéreux qui apporte une amélioration dans la vie des gens. Ma première piste se portaient sur les appareils de mesures météorologiques mais après avoir pris connaissances des projets des mes camarades, mon intéret s'est portés sur un des projets de Anaïs Derue : le casque feux arrière. Le casque feux arrière, utilisé par les cyclistes, est un feux permettant de signaler si le cycliste ralenti, va aller à gauche ou à droite et coûte [75 €](https://cosmoconnected.com/fr/produits-velo-trottinette/cosmo-ride).

![cosmo-ride](images/helmet.PNG)

Mon intéret sur ce produit est qu'il permettra, selon moi, d'améliorer la sécurité routière. J'ai vu récemment dans les journaux qu'il y a beaucoup d'accidents lié aux vélos et aux trottinettes et que les trottinettes électriques seront réglémenté de la même manière que les vélos. On peut aussi voir dans certains sites d'information comme [SudOuest](https://www.sudouest.fr/economie/transports/trottinettes-electriques-22-morts-et-au-moins-6-000-blesses-en-2021-des-chiffres-en-forte-hausse-8342063.php) ou [rtbf.be](https://www.rtbf.be/article/400-accidents-impliquant-des-trottinettes-electriques-a-bruxelles-en-2021-selon-vias-10939934) que le nombre d'accident impliquant les trottinette électriques augmentent. Avec un casque feux arrière, je pense que le nombre d'accidents diminuerait  car en le rendant accessible grâce à un prix abordable, plus de gens en auraient. Et on pense qu’on diminuant le risque d’accident, on incite plus à prendre le vélo et la trottinette électrique. Et l’utilisation de ces moyens de transport à place des voitures diminuerait la pollution liée à la production du CO2. Étant donné qu’ils sont moins émetteurs de CO2. Donc ce projet va être lié à l'objective 11 de développement durable qui porte sur des villes et communauté durables et à l’objective 13 par la diminution de CO2.

![](images/CO2_velo_voiture.png)


## Investigation

J'ai commencé mes recherches en regardant s' il n'existe pas déjà de projets existant ou proches du mien dans les sites de DiY (Do it yourself) comme Instructables ou Thingiverse.Après discussion avec Axel, il s'est avéré qu'il existe d'autres moyen d'avertissement des [cyclistes](https://www.amazon.fr/clignotant-v%C3%A9lo/s?k=clignotant+v%C3%A9lo) mais ces produits sont pricipalement pour des vélos. Sur Instructables, j'ai trouvé un projet intéressant qui se porte sur les LEDs [Neopixel](https://www.instructables.com/Helmet-Lights/). Ce projet m'a donné l'esquisse d'un premier prototype. Le premier produit Neopixel que je voulais utiliser était un [Adafruit NeoPixel Shield for Arduino](https://www.adafruit.com/product/2864) mais le prix est d'environ 30€ donc j'ai regardé d'autres produits moins chère. Mon autre idée est d'utiliser 2 [LED ring](https://fr.banggood.com/Ring-5V-16x-5050-RGB-LED-Board-with-Integrated-Drivers-Module-Geekcreit-for-Arduino-products-that-work-with-official-Arduino-boards-p-1199159.html?imageAb=1&cur_warehouse=CN&rmmds=detail-top-buytogether-auto&trace_id=dbb01650908611106&a=1651143710.7228&DCC=BE&currency=EUR&akmClientCountry=DE)  pour un total d'environ 8€.

Le matériel utilisé serait:

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Arduino         |  20.00 €| http://amazon.com/test   |        |
| 2   | Led ring        |  7.14 € | https://www.adafruit.com/product/1643   |        |
|1    | jumper wire pack|  2.49 €| https://www.cotubex.be/fr/fil-a-ponter/11412-jeu-de-fils-de-raccord-awg-1-broche-m-f-15cm-10pieces.html   |        |
| 1   | pile alcaline   |  3.75 €| https://www.cotubex.be/fr/piles-alcalines/3179-camelion-batterie-alcaline-9v-500mah.html   |        |
| 1   | module bluetooth  |  6.69 €| https://www.amazon.fr/azdelivery-Bluetooth-Transceiver-RS232-TTL-Communication/dp/B0722MD4FY/ref=pd_sbs_1_sccl_1/262-3686296-2499848?pd_rd_w=LdQMj&pf_rd_p=684ebbbd-2bae-46e5-b7e6-02f705bd0aa2&pf_rd_r=6C0A45QRVBGF754T10QW&pd_rd_r=693a8f05-4652-4e3d-ad5e-74d3b3d6d532&pd_rd_wg=oRpTW&pd_rd_i=B0722MD4FY&psc=1   |        |


Le prix total est d'environ 40€. Après discussion avec les autres, il est encore possible de réduire le coût et de l'innover.
Par exemple,pour réduire le coût, je peux utiliser le ATmega328P et non la carte Arduino entier ou bien un microcontrolleur moins chère.


### plan d'action

L'idée générale est de créer un dispositif de casque qui indique la prochaine direction prise par le cycliste ou l'utilisateur d'une trottinette Electrique. Je fais ce projet avec Alessandro Abbracciante.

Pour la création du prototype, différentes idées de fonctionnement des clignotants ont été pensées:

* Control par une télécommande ou le GSM

* Control par le GSM

* Système de clignotants par inclinaison de la tête

* Activation par des boutons sur les côtés du casque

Par élimination, celui qu’on a choisi est le système de clignotant par inclinaison de la tête.

Les raisons sont que le système par télécommande et gsm peut etre dangereux car il diminue l’attention du conducteur sur la route. Et les boutons sur les cotés du casque n’est pas vraiment pratique vu qu’on devra lâcher une main du guidon pour trouver le bouton ce qui peut faire perdre l’équilibre et donc mener à des accidents.


Après discussion avec Axel et Denis et des recherches, les composants électronique qu'on aurait besoin sont:

* un ATmega328P

* un capteur à inclinaison

* un breadboard

* NeoPixel Mini Button PCB

A la place d'acheter un arduino UNO qui coûte 20€, on peut acheter simplement son microcontrolleur,le ATmega328P, au prix de [3.20€](https://www.hdevbot.fr/circuits-integres/897-microcontroleur-atmega328p-pu.html).  Plusieurs tutoriels sont disponible pour le programmer:

* [lien 1](https://www.youtube.com/watch?v=uDUp4cLXFrY)

* [lien 2](https://www.youtube.com/watch?v=Miov8Kn8dDk)

Voici aussi un lien si vous voulez avoir accès à son [datasheet](https://www.mouser.be/datasheet/2/268/ATmega48A_PA_88A_PA_168A_PA_328_P_DS_DS40002061B-1900559.pdf)

Pour que le dispositif capte le mouvement de la tête, on utilise un capteur à inclinaison. Le capteur qu'on utilise a été fourni par le fablab ulb. La première chose qu'on a faite est de lire son [datasheet](https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-9150-Datasheet.pdf). Le datasheet précise une alimentation recommandé de 3.3V mais une alimentation en 5V est permise. Le capteur utilisé est un MPU-9150 à 9 degrée de libertés. Il comprend un accéléromètre à 3 axes, un gyroscope à 3 axes et un magnetomètre à 3 Axes. Sa connection avec le microcontrolleur se fait en Inter-IC Communication (I2C) par le biais des pins SDA et SCL. Ce produit fourni par le FabLab coute entre [15€](https://www.amazon.fr/SparkFun-acc%C3%A9l%C3%A9rom%C3%A8tre-mpu-9150-essieux-9150-pour/dp/B075XSBT3Y) et [20€](https://www.amazon.fr/Aihasd-MPU-9150-Mine-Axis-%C3%A9lectronique-Acc%C3%A9l%C3%A9rom%C3%A8tre/dp/B017J32P4Y) mais il peut être remplacer par un modèle bon marché le MPU-6050 qui coûte [3,90€](https://www.gotronic.fr/art-module-6-dof-sen-mpu6050-31492.htm). Le [MPU-6050](https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf) est un capteur à 6 degrée de libertés comprenant un accéléromètre à 3 axes et un gyroscope à 3 axes.

![MPU](images/MPU_red.jpg)

Il existe des bibliothèques pour pouvoir extraire ses données mais on a utilisé plutot un [code I2C](https://forum.arduino.cc/t/mpu6050-unstable-values-drift/624303/2) donnant de meilleur résultat:

```
// minimal MPU-6050 tilt and roll (sjr)
// works perfectly with GY-521, pitch and roll signs agree with arrows on sensor module 7/2019
// tested with eBay Pro Mini, **no external pullups on SDA and SCL** (works with internal pullups!)
//
#include<Wire.h>
const int MPU_addr1 = 0x68;
float xa, ya, za, roll, pitch;

void setup() {

  Wire.begin();                                      //begin the wire communication
  Wire.beginTransmission(MPU_addr1);                 //begin, send the slave adress (in this case 68)
  Wire.write(0x6B);                                  //make the reset (place a 0 into the 6B register)
  Wire.write(0);
  Wire.endTransmission(true);                        //end the transmission
  Serial.begin(9600);
}

void loop() {

  Wire.beginTransmission(MPU_addr1);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr1, 6, true); //get six bytes accelerometer data

  xa = Wire.read() << 8 | Wire.read();
  ya = Wire.read() << 8 | Wire.read();
  za = Wire.read() << 8 | Wire.read();

  roll = atan2(ya , za) * 180.0 / PI;
  pitch = atan2(-xa , sqrt(ya * ya + za * za)) * 180.0 / PI;

  Serial.print("roll = ");
  Serial.print(roll,1);
  Serial.print(", pitch = ");
  Serial.println(pitch,1);
  delay(400);
}

```

Pour les Leds, on utilise des NeoPixel Mini Button PCB déja present au fablab.

![Neopixel](images/Neopixel_red.jpg)

Ces leds sont au prix de [$4.95](https://www.adafruit.com/product/1612). Ces NeoPixel sont des leds RGB permettant ainsi de faire voir une palette de couleurs. Il existe une bibliothèque permettant d'intéragir avec les produits [NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel). Pour verifier si ils marchent, nous avons utiliser ce code.

```
// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// Released under the GPLv3 license to match the rest of the
// Adafruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN        5 // On Trinket or Gemma, suggest changing this to 1

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 3 // Popular NeoPixel ring size

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 500 // Time (in milliseconds) to pause between pixels

void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
}

void loop() {
  pixels.clear(); // Set all pixel colors to 'off'

  // The first NeoPixel in a strand is #0, second is 1, all the way up
  // to the count of pixels minus one.
  for(int i=0; i<NUMPIXELS; i++) { // For each pixel...

    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    pixels.setPixelColor(i, pixels.Color(250, 0, 0));

    pixels.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
  }
}

```

Donc, voici un tableau recapulatif des composants:

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | ATmega328P      |  3.20 €| https://www.hdevbot.fr/circuits-integres/897-microcontroleur-atmega328p-pu.html   |        |
| 1   | MPU-6050        |  3.90 € | https://www.gotronic.fr/art-module-6-dof-sen-mpu6050-31492.htm   |        |
| 5   | Neopoxel Mini Button PCB|  5 € (pack de 5)| https://www.adafruit.com/product/1612   |        |
| 1   | Kit breadboard + cable  |  5.90 €| https://www.kubii.fr/kit-de-composants/1958-kit-breadboard-700-et-cables-jumper-kubii-3272496008205.html |        |
| 1   | Quartz 16 MHz  |  0.95 €| https://euro-makers.com/fr/accessoires-arduino/1856-httpeuro-makerscomcomposants-electronique1856-cristal-oscillateur-a-quartz-16mhz-3701172907995html-3701172907995.html   |        |

La somme total vaut plus ou moins 20€

Voici le schéma électronique qu'on devrait avoir:

![circuit électronique](images/circuit_elec.PNG)

A partir de ce circuit, il est possible de concevoir un PCB customisé.

##Act

Après avoir vérifier que les composants marchaient, nous avons construit un circuit pour connecter les leds NeoPixel et le MPU-9150 au microcontrolleur. Nous avons créer un premier code pour que les LEDs s'allument quand on penche d'un coté à un certains degrée:

```
/*
    FILE    : proto_code.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com> and Alessandro Abbracciante <alessandro.abbracciante@ulb.be>

    DATE    : 2022-05-12

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : minimal MPU-6050 tilt and roll (sjr) from https://forum.arduino.cc/t/mpu6050-unstable-values-drift/624303/2

    and NeoPixel Ring simple sketch from  Adafruit NeoPixel library

*/


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#include<Wire.h>
const int MPU_addr1 = 0x68;
float xa, ya, za, roll, pitch, pre_pitch;
unsigned long start;
float limit_right;
float limit_left;

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 3 // Popular NeoPixel ring size

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN_red        6 // On Trinket or Gemma, suggest changing this to 1
#define PIN_green        5 // On Trinket or Gemma, suggest changing this to 1

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_red, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN_green, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 500 // Time (in milliseconds) to pause between pixels


void setup() {
  // put your setup code here, to run once:

  Wire.begin();                                      //begin the wire communication
  Wire.beginTransmission(MPU_addr1);                 //begin, send the slave adress (in this case 68)
  Wire.write(0x6B);                                  //make the reset (place a 0 into the 6B register)
  Wire.write(0);
  Wire.endTransmission(true);                        //end the transmission
  Serial.begin(9600);

  pre_pitch=0.0;

  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

}

void loop() {
  // put your main code here, to run repeatedly:
  pixels.setPixelColor(0, pixels.Color(0, 0, 0));
  pixels.setPixelColor(1, pixels.Color(0, 0, 0));
  pixels.setPixelColor(2, pixels.Color(0, 0, 0));
  pixels.show();   // Send the updated pixel colors to the hardware.

  strip.setPixelColor(0, pixels.Color(0, 0, 0));
  strip.setPixelColor(1, pixels.Color(0, 0, 0));
  strip.setPixelColor(2, pixels.Color(0, 0, 0));
  strip.show();   // Send the updated pixel colors to the hardware.

  Wire.beginTransmission(MPU_addr1);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr1, 6, true); //get six bytes accelerometer data

  xa = Wire.read() << 8 | Wire.read();
  ya = Wire.read() << 8 | Wire.read();
  za = Wire.read() << 8 | Wire.read();

  roll = atan2(ya , za) * 180.0 / PI;
  pitch = atan2(-xa , sqrt(ya * ya + za * za)) * 180.0 / PI;

  Serial.print("roll = ");
  Serial.print(roll,1);
  Serial.print(", pitch = ");
  Serial.println(pitch,1);

  limit_right=40;

  stop_clignotant=10000; // 10 second

  if (pitch-pre_pitch >limit_right){
      start = millis();
      while (millis() - start <stop_clignotant){
        Serial.println(millis() - start);
        pixels.setPixelColor(0, pixels.Color(250, 0, 0));
        //pixels.show();   // Send the updated pixel colors to the hardware.
        pixels.setPixelColor(1, pixels.Color(250, 0, 0));
        //pixels.show();   // Send the updated pixel colors to the hardware.
        pixels.setPixelColor(2, pixels.Color(250, 0, 0));
        pixels.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL); // Pause before next pass through loop
        pixels.setPixelColor(0, pixels.Color(0, 0, 0));
        pixels.setPixelColor(1, pixels.Color(0, 0, 0));
        pixels.setPixelColor(2, pixels.Color(0, 0, 0));
        pixels.show();   // Send the updated pixel colors to the hardware.
        delay(DELAYVAL); // Pause before next pass through loop
      }
  }

  limit_left=40;

  if (pitch-pre_pitch <-limit_left){
      start = millis();
      while (millis() - start <stop_clignotant){
        Serial.println(millis() - start);
        strip.setPixelColor(0, pixels.Color(0, 250, 0));
        strip.setPixelColor(1, pixels.Color(0, 250, 0));
        strip.setPixelColor(2, pixels.Color(0, 250, 0));
        strip.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL); // Pause before next pass through loop
        strip.setPixelColor(0, pixels.Color(0, 0, 0));
        strip.setPixelColor(1, pixels.Color(0, 0, 0));
        strip.setPixelColor(2, pixels.Color(0, 0, 0));
        strip.show();   // Send the updated pixel colors to the hardware.
        delay(DELAYVAL); // Pause before next pass through loop
      }
  }

  delay(400);

}

```

Voici une video:

<video width="500"  controls muted>
<source src="../images/leds_test_show.mp4" type="video/mp4">
</video>

![ Si la video ne marche pas](docs/images/leds_test_show.mp4)[,cliquez sur ce lien](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/blob/main/docs/images/Leds_test_show.mp4)

Le problème de ce code est qu'en cas d'erreur de clignotant de direction, il est impossible de changer avant la fin du temps imparti (stop_clignotant). Donc une autre version du code a été crée pour remédier à ce problème.

```
/*
    FILE    : proto_code_V2.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com> and Alessandro Abbracciante <alessandro.abbracciante@ulb.be>

    DATE    : 2022-05-12

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : minimal MPU-6050 tilt and roll (sjr) from https://forum.arduino.cc/t/mpu6050-unstable-values-drift/624303/2

    and NeoPixel Ring simple sketch from  Adafruit NeoPixel library

*/

#include <Adafruit_NeoPixel.h>
#include <Wire.h>

#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif


// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 3 // Popular NeoPixel ring size

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN_red        6 // On Trinket or Gemma, suggest changing this to 1
#define PIN_green        5 // On Trinket or Gemma, suggest changing this to 1

#define DELAYVAL 500 // Time (in milliseconds) to pause between pixels
#define BLINK_TIME 10000 // blink time in ms


// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_red, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN_green, NEO_GRB + NEO_KHZ800);


const int MPU_addr1 = 0x68;
float xa, ya, za, roll, pitch, pre_pitch;
unsigned long start_time;
float limit_right;
float limit_left;
bool tilt_left = false;
bool tilt_right = false;
int counter = BLINK_TIME;


void setup() {
  // put your setup code here, to run once:

  Wire.begin();                                      //begin the wire communication
  Wire.beginTransmission(MPU_addr1);                 //begin, send the slave adress (in this case 68)
  Wire.write(0x6B);                                  //make the reset (place a 0 into the 6B register)
  Wire.write(0);
  Wire.endTransmission(true);                        //end the transmission
  Serial.begin(9600);

  pre_pitch=0.0;

  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
  #endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

  pixels.clear();
  pixels.show();   // Send the updated pixel colors to the hardware.

  strip.clear();
  strip.show();   // Send the updated pixel colors to the hardware.

}

void loop() {
  // put your main code here, to run repeatedly:

  start_time = millis(); // start couting the loop duration

  Wire.beginTransmission(MPU_addr1);
  Wire.write(0x3B);
  Wire.endTransmission(false);

  Wire.requestFrom(MPU_addr1, 6, true); //get six bytes accelerometer data
  xa = Wire.read() << 8 | Wire.read();
  ya = Wire.read() << 8 | Wire.read();
  za = Wire.read() << 8 | Wire.read();

  roll = atan2(ya , za) * 180.0 / PI;
  pitch = atan2(-xa , sqrt(ya * ya + za * za)) * 180.0 / PI;

  Serial.print("roll = ");
  Serial.print(roll,1);
  Serial.print(", pitch = ");
  Serial.println(pitch,1);

  limit_right=40;
  limit_left=40;

  if (pitch-pre_pitch >limit_right){
      tilt_right = true;
      tilt_left = false; // stop left led if it was on
      counter = BLINK_TIME; // reset counter if other led was on

  }


  if (pitch-pre_pitch <-limit_left){
      tilt_left = true;
      tilt_right = false; // stop right led if it was on
      counter = BLINK_TIME; // reset counter if other led was on

  }

  if(tilt_left){
    strip.setPixelColor(0, pixels.Color(250,0,0));
    strip.setPixelColor(1, pixels.Color(250,0,0));
    strip.setPixelColor(2, pixels.Color(250,0,0));
    strip.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    strip.clear();
    strip.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    counter -= 400 + millis() - start_time;
  }

  if(tilt_right){
    pixels.setPixelColor(0, pixels.Color(0,250,0));
    pixels.setPixelColor(1, pixels.Color(0,250,0));
    pixels.setPixelColor(2, pixels.Color(0,250,0));
    pixels.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    pixels.clear();
    pixels.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    counter -= 400 + millis() - start_time;
  }

  if (counter <= 0) {
    counter = BLINK_TIME;
    tilt_left = false;
    tilt_right = false;
  }


  delay(400);

}

```

### Mise en place du système sur un casque

Après avoir fait le circuit électronique, il reste plus qu'à trouver un moyen de le placer sur un casque de cycliste. Le plus simple est d'acheter une boîte et de placer le circuit à l'intérieur comme ce [tuto d'Instructable](https://www.instructables.com/Helmet-Lights/):

![](images/casque_mount.PNG)


et les Leds seraient maintenu par du scotch double-face.

Mais dans le cadre de ce cours qui nous encourage à utiliser les outils du fablab, nous prévoyons de créer notre propre boîte en bois à l'aide d'une découpeuse laser.  

La première étape est d'esquisser les différentes parties qui composeront notre boîte. Pour cela, nous avons utiliser SolidWorks qu'on a ensuite transformer le fichier obtenu en un fichier svg. Nous avons esquisser 3 pièces: la base, les cotés le long de la longueur et les cotés le long de la largeur.

Voici des images des parties sur InkScape:    

Base:

![](images/Base.PNG)

Cotés le long de la largeur:

![](images/large_side.PNG)

Cotés le long de la longueur:

![](images/L_side.PNG)

Le moyen de connection entre la base et les cotés le long de la longueur se fait grâce avec des vis et écrous. La vis passe par la base et l'encoche du coté et ensuite on place un écrou qu'on sert avec un tournevis. Pour les cotés le long de la largeur, c'est simplement par embriquement.

Pour les premières pièces imprimés, on avait un problème de dimensionnement. On l'a corrigé grâce aux fonctions de InkScape et que vous pouvez avoir accès grâce à ce [lien](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/blob/main/docs/Project_files/tout.svg).

![](images/partie_separe.jpg)

![](images/assembly_1.jpg)

![](images/assembly_2.jpg)


Le matériau utilisé pour ce prototype est du carton d'épaisseur fine ce qui le rend fragile pour serrer la base et les cotés ensembles. Il faudrait mieux utilisée du bois avec une épaisseur plus grande. Mais malheureusement dû à un manque de temps, nous avons pas plus perfectionner plus loin notre prototype de boîte. Les faces de ces pièces sont vierges laissant ainsi de la place pour graver des dessins et donc de personnaliser la boîte créer.

Tous les fichier ino et svg crées et utilisées durant ce projet sont dans ce [lien](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/tree/main/docs/Project_files)   
