/*
  FILE    : Blink_atmel.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-31
    
    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Adapted from basics examples of the arduino ide.

*/
int led=15;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);                       // wait for a second
}
