/*
    FILE    : read_temperature.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-17

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : AnalogReadSerial of  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/



void setup() {
  // put your setup code here, to run once:
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);

}
