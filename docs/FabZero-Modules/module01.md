# 1. Documentation

## Bonnes pratiques

Durant le premier cours de Fabzero, il a été dit que l'utilisation d'un système de control de version (version control system) est l'outil le plus puissant pour la documentation d'un projet et le partage de ce document avec nos collaborateurs. Parce que ce système fait une sauvegarde automatique des version et modifications faites possédant donc une option "historique". Grâce à ça, par exemple, durant une collaboration, si un collaborateur efface par mégarde le texte d'un autre, il suffit de juste de cliquer sur historique pour le revoir.

L'outil utilisé pour ce cours est gitlab, un open-source version control sytem

## Linux

Dans le cas où on souhaite modifier nos documents localement et non sur le site web, on peut communiquer ou transférer entre le bureau de notre ordi et le site gitlab via l'invite de commande avec l'Unix shell Bash.

![](../images/bash.PNG)

Possédant le système d'exploitation Windows 10, il est déja présent dans mon ordinateur. Pour l'activer,j'ai suivi ce site [Ineat Blog](https://blog.ineat-group.com/2020/02/utiliser-le-terminal-bash-natif-dans-windows-10/) me permettant d'utiliser des commandes Linux.


## Markdown

Gitlab utilise le format Markdown pour la création de texte étant plus simple que HTML. HTML est un format nécessitant de faire attention aux balise de code comparé à markdown qui se concentre sur le texte en utilisant des symboles pour donner des effets à nos mots d'une manière facile et simple et on peut exporter notre texte facilement au format HTML. Pour la liste des symboles aller voir à ce lien [Markdown](https://en.wikipedia.org/wiki/Markdown)

J'ai installer 3 software qui permet d'effectuer différentes action sur les fichiers:

* Pandoc : pour la conversion en format HTML

* Graphicsmagick : pour l'édition d'images

* FFMPEG : pour compression de videos

Graphicsmagick permet des actions comme changer la taille d'une image avec la commande:

```
$ gm convert -resize 600x600 {nom du fichier} {nom du fichier avec les nouvelles dimension}
```

ou d'autres commande que vous pouvez trouvez à ce [lien](http://www.graphicsmagick.org/utilities.html)

FFMPEG permet lui des actions comme compresser la taille d'une video avec la commande:

```
ffmpeg -i input_video -vcodec libx264 -crf 25 -preset medium -vf scale=-2:1080 -acodec libmp3lame -q:a 4 -ar 48000 -ac 2 output_video.mp4
```
ou d'autres commande que vous pouvez trouvez à ce [lien](http://academy.cba.mit.edu/classes/computer_design/video.html)

Ces commandes dans le bash doivent être effectuer dans le répertoire où se trouve le fichier pour qu'ils fonctionnent.

## Clonage d'un git projet par une connexion ssh

Il est possible de cloner un projet de gitlab vers notre ordinateur par le biais d'une connexion ssh (secure shell). Pour créer cette connexion,j'ai suivi les indications de ce [site web](https://www.youtube.com/watch?v=Vmt0V6a3ppE). Il faut donc commencer par créer une clé public en utilisant cette commande:


```
$ ssh-keygen
```

Après avoir créer le fichier contenant la clé public (précisé par un .pub), vous devez utilisé la commande "cat" pour montrer le contenu du fichier et copier-coller dans gitlab comme dans la video.

Et ainsi,vous pouvez cloner votre projet dans la mémoire de votre ordinateur avec git clone.

```
$ git clone <name_of_the_ssh_link>
```
## Git push et Git pull

En plus de git clone, il existe 2 autres commandes git:

* git pull
* git push

Le premier, git pull, permet de prendre la version à jour du projet clone en tapant cette commande directement dans le dossier contenant tous les fichiers du projet (.git,doc,...) avec la commande 'cd'.

La deuxième commande est git push qui permet de envoyer toutes modifications faites venant de votre projet local à votre projet git mais attention, vous devez au préalable préparer vos fichiers pour un commit avec soit:

```
$ git add <file-name OR folder-name>
$ git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
```

pour tous les fichiers un par un.

ou 2 autres Choix:

```
$ git add -A //prend tous les fichiers modifiés
$ git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
```
ou
```
$ git commit -a -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
```
pour les préparer tous en même temps.

et ensuite vous faites git push.

Pour vérifier que tous est en ordre,vous pouvez faire:

```
$ git status
```

qui vous permet de voir les fichiers qui ont besoin d'être charger par un git add.

![](../images/status.PNG)

Pour plus d'informations,voici 2 liens:

* [Configuring git on windows, understanding git clone, pull, private and public repositories](https://www.youtube.com/watch?v=VzuTbx2MhnE)

* [Git commit and Push with complete git flow](https://www.youtube.com/watch?v=Bkk1aVKccAs)
