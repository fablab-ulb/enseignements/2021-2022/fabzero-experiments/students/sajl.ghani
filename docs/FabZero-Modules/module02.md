# 2. Conception Assistée par Ordinateur

Pour le module 2 du cours, il y a eu une présentation de "Qu'est-ce qu'un mécanisme flexible (compliant mechanism)". Un mécanisme flexible est un mécanisme où les articulations rigides permettant les transfert de forces ou de déplacement sont remplacés par une déformation élastique. Ces déformations élastiques permettent une rotation autour un centre ou des translations horizontales ou verticales.

Pour ce module, on nous est demandé de concevoir un  FlexLinks avec un logiciel de Conception Assistée par Ordinateur (CAD: computer-aided design). Les logiciels qu'on devait utilisés sont:

* OpenSCAD: un logiciel de compilation de modèles 3D par script informatique.

* FreeCAD: un logiciel de modélisation 3D d'objet open-source

Le modèle FlexLink doit être documenter et répresenter par code paramétriques (les paramétres sont des variables en dehors des fonctions) en cas d'ajustement des propriètés du matériaux ou des caractéristiques de la machine.      

## OpenSCAD

Pour mon premier modèle, j'ai utilisé le logiciel OpenSCAD. OpenSCAD est un logiciel de modélisation par création d'un script. C'est un logiciel open source muni de function intégré qu'on peut trouver dans cette liste [CheatSheet](https://openscad.org/cheatsheet/) et de bibliothèques accessible sur github comme [BOSL](https://github.com/revarbat/BOSL)

### FlexLink: fixed-fixed beam

Avant de commencer mon code, j'ai cherché les dimensions standard d'un lego que j'ai trouvé sur ce [site](https://fr.wikipedia.org/wiki/Fichier:Lego_dimensions.svg). Le FlexLink que j'ai crée est un simple fixed-fixed beam modélisé à partir de code que j'ai crée:  

```
/*
    FILE    : flexlink.scad

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-02-21

*/

//parametres
$fn=100;

width=15.8;//mm représente la longueur du bout ovale
depth=7; //mm  représente la largeur du bout ovale
height=3.2;//mm représente la hauteur du bout ovale
length_br=50;//mm la longueur de la branche reliant les 2 extrémités

hole_r=2.4; // le rayon du trou
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous

difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);

}//Création de l'extrémité avec les trous



translate([width,depth/2,0])cube([length_br, depth/8,height]);//création de la tige de connexion

translate([width+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};//création de la deuxième extrémité
```

Les dimensions sont représenté de manières paramètrique permettant facilement de les ajuster en cas de problèmes. Il est conseillé de commencer cette habitude au tout début.

![](../images/Openscad_beam.PNG)

## FreeCAD

Pour avoir une large experience sur les différent logiciels de Conception Assistée par Ordinateur, j'ai créé un deuxième modèle avec le logiciel FreeCAD. FreeCAD est un modeleur 3D paramétriques interactive. Nous pouvons directement dessiner notre modèle dans le logiciel et mettre les dimensions de notre pièce dans un spreadsheet qui nous permet de les utiliser lors d'application de contraintes.

### FlexLink: Cross-Axis Flexural Pivot

Pour mon deuxième modèle,j'ai voulu prendre celui qui me paraissait difficile à construire avec OpenSCAD donc j'ai pris Cross-Axis Flexural Pivot.

J'ai commencé par créer le corps de la pièce avant d'y mettre des trous. Voici les étapes de ma conception.

j'ai fait un sketch de la pièce avec les dimensions suivantes.

![](../images/sketch_crossx.PNG)
![](../images/Dimension_cross.PNG)

puis j'ai ensuite dessiner les trous et fait des cavités

![](../images/sketch2_crossx.PNG)
![](../images/crossX.PNG)

FreeCAD est plus facile à manier étant donné qu'on peut dessiner directement notre modèle dans l'interface. Pour ceux qui ont déja utilisé SolidWorks ou Inventor,ce sera intuitive.

* Voici un lien pour avoir accès à mes codes sources: [module 2 files](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/tree/main/docs/module_2_files)
