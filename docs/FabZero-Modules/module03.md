# 3. Découpe assistée par ordinateur

La découpeuse laser est une découpeuse qui permet des découpes très précises mais l'utilisation de ces machines peuvent être très dangereuse si certaines consignes de sécurité ne sont pas respectés.

## Les précautions à absolument suivre.

### Pour éviter tout risque d'incendie

* Toujours activer l’air comprimé !
* Toujours allumer l’extracteur de fumée !
* Savoir où est le bouton d’arrêt d’urgence
* Savoir où trouver un extincteur au CO2
* Rester à proximité de la machine jusqu'à la fin de la découpe

### Pour votre santé

* Ne pas regarder fixement l'impact du faisceau LASER
* Après une découpe, ne pas ouvrir la machine tant qu'il y a de la fumée à l'intérieur

Une autre précaution à absolument prendre en compte est de connaître avec certitude quel matériau est utilisé

## Les matériaux

### Matériaux recommandés

* Bois contreplaqué (plywood / multiplex)
* Acrylique (PMMA / Plexiglass)
* Papier, carton
* Textiles

### Matériaux déconseillés

* MDF : fumée épaisse, et très nocive à long terme
* ABS, PS : fond facilement, fumée nocive
* PE, PET, PP : fond facilement
* Composites à base de fibres : poussières très nocives
* Métaux : impossible à découper

### Matériaux interdits !

* PVC : fumée acide, et très nocive
* Cuivre : réfléchit totalement le LASER
* Téflon (PTFE) : fumée acide, et très nocive
* Résine phénolique, époxy : fumée très nocive
* Vinyl, simili-cuir : peut contenir de la chlorine
* Cuir animal : juste à cause de l'odeur

## Présentation des machines

Le Fablab ULB posséde 3 découpeuse laser décrit comme suit:

### Epilog Fusion Pro 32

Spécifications:

* Surface de découpe : 81 x 50 cm
* Hauteur maximum : 31 cm
* Puissance du LASER : 60 W
* Type de LASER : Tube CO2 (infrarouge)

![](../images/epilog_laser.jpg)

cette découpeuse est muni d'une caméra en dessous de son capot permettant de placer notre esquisse précisement sur le matériau en suivant au préalable les étapes du [manuel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md).

### Lasersaur

Spécifications:

* Surface de découpe : 122 x 61 cm
* Hauteur maximum : 12 cm
* Vitesse maximum : 6000 mm/min
* Puissance du LASER : 100 W
* Type de LASER : Tube CO2 (infrarouge)

![](../images/lasersaur_image.jpg)

Cette découpeuse est controlées par l'interface DriveBoard qui traite les format DXF, SVG et DBA mais chacun à [des inconvénients et des avantages](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md). Pour la procédure d'utilisation,suivez ce [lien](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)

### Full Spectrum Muse

Spécifications:

* Surface de découpe : 50 x 30 cm
* Hauteur maximum : 6 cm
* Puissance du LASER : 40 W
* Type de LASER : Tube CO2 (infrarouge) + pointeur rouge

![](../images/muse_image.jpg)

Cette découpeuse est controlées par l'interface Retina Engrave qu'on peut se connecter de 2 manières:

* connectez votre pc à la machine par cable ethernet.
* utilisez le routeur wifi dédié: Wifi => LaserCutter & MDP : fablabULB2019

Une fois la connexion établie, entrez l'adresse http://fsl.local dans votre navigateur.

Formats de fichier pris en charge :

* Vectoriel : SVG → Le fichier sera importé en vectoriel mais aussi en matriciel, il faudra donc en supprimer un des deux
* Matriciel : BMP, JPEG, PNG, TIFF (pour engraver)
* PDF (vectoriel et matriciel)

Pour la procédure d'utilisation, on nous a conseillé de suivre ces [tutoriels vidéo](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6).

## Travail de Groupe : calibration

Pour ce cours, il a été demandé de former un groupe autour d'une des découpeuse laser. J'ai formé un groupe avec Alessandro ABBRACCIANTE, Mats BOURGEOIS et Alexandre STOESSER.

Pour obtenir une bonne découpe du matériau, il est important de bien choisir les valeurs données au paramètres du laser:

* Puissance du laser
* vitesse de découpe du laser

Il est aussi important de régler la hauteur par rapport à la surface du matériau pour avoir un bon focus (le point de croissement) sur la surface externe. La profondeur et le kerf (diamètre du rayon du laser) dépend des valeurs des paramètres donc il est nécessaire de faire une table de calibration:

 ![](../images/calibration_image.jpg)

 Cette calibration a été faites avec le Lasersaur dont la hauteur du laser est de 15mm.



## Travail individuel : kirigami

En plus du travail de groupe, on nous est demandé de faire un travail individuel. Ce travail consiste à faire un kirigami. Le kirigami est un morceau découpé par la découpeuse laser qu'on plie ensuite pour obtenir un objet désiré.

L'objet que j'ai choisi de formé est une simple boite inspiré de ce model:

![](../images/box_proto.PNG)
[lien](https://ro.pinterest.com/pin/389913280237142103/)

 A partir de ce modèle, j'ai crée ma propre version avec Inkscape. j'ai commencé par faire un assemblage de rectangle puis ensuite j'ai repassé sur les lignes avec 2 couleurs différentes: rouge pour la gravure et noir pour la découpe.

![](../images/box_esquisse.PNG)

* Pour connaitre les dimensions que j'ai utilisée, allez à ce [lien](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/tree/main/docs/laser_cutting_files)

Pour avoir une large expérience pratique des machines, j'ai décidé d'utiliser l'epilog.

Pour l'utiliser, j'ai importer mon fichier svg sur le pc de la machine grâce à une clé USB et ensuite, j'ai allumer les 2 machines suivantes:

* extracteur de fumée

![](../images/extract_fumer.jpg)

* l'EpilogFusion

![](../images/epilog_menu.jpg)

Pour allumer l'EpilogFusion, on appuie sur l'interrupteur au-dessus et ensuite, on tourne la clé horizontalement.

Après l'avoir allumer, j'ai ouvert son dashboard en cliquant sur Fichier>imprimer d'Inkscape. Sur le dashboard,j'ai splitté mon esquisse par couleurs:

* l'engrave (rouge)

![](../images/engrave.jpg)

j'ai précisé que le type de processus est engrave avec une vitesse de 90% et une puissance de 15%

* la découpe (noir)

![](../images/vector.jpg)

j'ai bien précisé aussi que le type de processus est vector avec une vitesse de 75% et une puissance de 25% mais aussi une fréquence de 100%

Pour les valeurs des paramètres, je me suis basé sur le test de calibration de [Pauline Mackelbert](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module03/)

Et pour finir, je place bien mon dessin sur le matériau grâce au dashboard.

![](../images/select_place.jpg)

Et voila, ce que j'obtiens:

![](../images/object_obtained.jpg)
![](../images/box.jpg)
