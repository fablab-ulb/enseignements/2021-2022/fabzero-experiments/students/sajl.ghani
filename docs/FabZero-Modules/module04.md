# 4. Impression 3D

Cette semaine, pour ce module, on nous a enseigné les avantages et les limites d'utiliser une imprimante 3D et les procédés pour l'utilisez. Pour bien comprendre l'importance d'une imprimante 3D, un invité spécial a présenté les capacités incroyables de l'imprimante 3D et de ses applications

## Applications de l'imprimante 3D  

L'imprimante 3D s'applique dans plusieurs secteurs:

* Transport et aviation: créer des pièces légères pour diminuer le poids des véhicules et augmenter leurs puissances

* Biens de consommation

* Médecines (domaine de prédilection de l'invité): médicament plus adaptés aux patients ou ils se solvent plus rapidement.

## Type d'imprimante 3D

Il existe différents procédés pour imprimer un objet en 3D:

* lit de poudre (drop-on powder deposition,en anglais)

* goutte-sur-goutte

* micro-seringue pressurisées (utiliser principalement dans la patisserie)

* Stéréolithographie

* digital light processing

* dépot du fondu (méthode utilisée par les PrusaSlicer)

Etant donné que le PrusaSlicer utilie le procédé de dépot de fondu, il nécessite différents pré-traitement et post-traitement.

## code licenses

Lors du module de conception assistée par ordinateur, il a été demandé de créer un code OpenSCAD et maintenant, on doit spécifier ses propriètés: le nom du fichier, l'auteur, sa date de création et les permissions accordées ou non.

```
/*
    FILE    : flexlink.scad

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-02-21

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

*/

//parametres
$fn=100;

width=15.8;//mm représente la longueur du bout ovale
depth=7; //mm  représente la largeur du bout ovale
height=3.2;//mm représente la hauteur du bout ovale
length_br=50;//mm la longueur de la branche reliant les 2 extrémités

hole_r=2.4; // le rayon du trou
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous

difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);

}//Création de l'extrémité avec les trous



translate([width,depth/2,0])cube([length_br, depth/8,height]);//création de la tige de connexion

translate([width+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};//création de la deuxième extrémité
```

![](../images/flex_print.jpg)

La license précisé indique que mon code peut être partager tant que c'est en dehors d'objectives commercial.

Pour voir les autres types de licenses, cliquer sur ce [lien](https://creativecommons.org/about/cclicenses/)

## Imprimer des objets 3D

Pour ce module, on nous a montrés comment utiliser le logiciel PrusaSlicer. Le PrusaSlicer permet de régler les caractéristiques de l'impression: la hauteur des couches, le remplissages, la coque,... .

### Etape d'impression:

1. savoir le type d'imprimante utilisé. Celui qu'on va utiliser est:

Prusa I3MK3S:

-	Taille du plateau d’impression : **22,5cm x 22,5cm**
-	Hauteur d’impression maximale : **25 cm**
-	Diamètre de buse (au FabLab ULB) : **0,4mm**

Important à connaitre et les mettres dans les réglages.

2. importer votre model en format STL ou OBJ

3. Choississez la bonne direction d'impression

4. il est conseillé de se mettre en mode expert pour avoir plus d'option

5. mettez les bonnes valeurs pour les caratéristiques des couches et de la coque.

6. prenez la densité et le motif de remplissage le plus adaptés pour votre objet.

7. régler le filament pour correspondre à celui que vous allez utilisé.

8. générer votre G-code et importer le dans une carte SD.

9. mettre votre carte SD contenant votre modèle dans l'imprimante 3D

10. sélectionner votre fichier et lancez l'impression

Pour plus de détail sur les étapes, allez à ce [lien](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md)

### Précaution

Même si l'impression 3D permet de créer des objets complexes et variés, il est conseillé de l'utiliser que quand c'est le seul moyen car:

* l'impression prend enormément de temps en fonction de l'objet.

* il est fragile. un petit mauvais réglage peut abimer la pièce durant l'impression: le fil de la bobine est bloqué, le plateau est couvert de poussière, la pièce s'est décroché,...    

## Travail de groupe : torture test

 Parmi les tâches à faire, un travail de groupe doit être fait et qui consiste à imprimer un torture test. Le site fablab donne un lien qui permet de télécharger le stl d'un [torture test](https://www.thingiverse.com/thing:2806295).

 Les paramètres choisis sont les mêmes que dans [le lien des précautions](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md) avec comme différence, le motif de remplissage est gyroide
, on enlève le support et on diminue les dimensions à 80% (temps d'impression:1H43) pour un temps d'impression plus court (100%=2H50)

et on a obtenu, ce torture test:

![](../images/test_face.jpg)
![](../images/test_droit.jpg)
![](../images/test_gauche.jpg)

On peut voir que la plupart des tests ont été réussi par l'imprimante 3D à part le springing test:

![](../images/stringing.png)

Les petites collonnes sont très fragile. on ne sait pas quand mais ces collonnes se sont décollé du torture test pendant un transfert. Pour rappel, on a redimensionné à 80% la taille d'origine donc on peut faire comme hypothèse qu'avec un épaisseur de colonnes inférieur au diamètre de la buse, un élément peut facilement se casser. Le overhang test est réussi aussi à part que l'arrière est moins lisse.

## Travail individuel : compliant mechanism

L'autre tâche à faire est un travail individuel. Le travail consiste à faire un mécanisme flexible avec des Flexlinks. Le mécanisme que j'ai visé à faire est la chose suivante:

![](../images/bistables.jpg)

Je voulais faire une version simplifié à base de flexlink. La première chose que j'ai faites est de chercher la taille des trous à faire en créant un modèle OpenSCAD

```
/*
    FILE    : test_hole.scad

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-29

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

*/

$fn=100;

width=15.8;//mm the 2 extremities
depth=7;
height=3.2;

hole_r=2.6;
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous

difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width+40,depth/2,0])cylinder(h=height,r=depth/2);
}

for (i = [0:6]) {

    translate([(width/4+(dist_hole*i)),depth/2,0])cylinder(h=height*2,r=hole_r-(0.1*i));

    translate([(width/4+(dist_hole*i)),depth/2,eps])cylinder(h=height*2,r=hole_r-(0.1*i));

}

}
```

![](../images/hole.PNG)

![](../images/hole_print.jpg)

A partir de ce modèle, le rayon à avoir est de 2.5mm.

Pour mon modèle, je pensais à utiliser le flexlinks de Paul Bryssinck

```
/*
    FILE    : cantileverBeam.scad

    AUTHOR  : Paul Bryssinck <paulbryssinck@hotmail.com>

    DATE    : 2021-02-27

    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/

    Original design by the BYU Compliant Mechanisms Research Group (CMR) (https://www.compliantmechanisms.byu.edu/flexlinks)
*/

//parametres
$fn=100;
pi=3.1415926535;
thick=3; //epaisseur de la piece (direction z)
//fixation:
nHoles=2;
rOut=4; //largeur fixation (direction y) (la longueur de la fixation est nHoles*2*rOut)
rInn=2.5; //taille des trous
holeSep=0;//pour controler l'espacement entre les trous
//liaison
linkLen=20; //longueur de la tige depliee
linkWid=1.5;
r=2*linkLen/pi;
linkOutRad=r+linkWid/2;
linkInnRad=r-linkWid/2;

//coeur de la fixation :
module filledClip(xPos,yPos,zPos){
    hull(){
        cylinder(h=thick, r=rOut);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=rOut);
        }
}

//liaison :
module link(xPos,yPos,zPos){
    difference(){
        translate([xPos,yPos,zPos])cylinder(h=thick, r=linkOutRad);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=linkInnRad);
        }
}

//Assemblage et perçage des différentes parties
module assembly(){
    union(){
        //fixation 1 (touchant l'axe y)
        translate([-rOut,-r,0])rotate([0,0,180])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick+10,r=rInn);
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick-50,r=rInn);
            }
        }
        //fixation 2 (touchant l'axe x)
        translate([+r,rOut,0])rotate([0,0,90])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick+10,r=rInn);
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick-50,r=rInn);
            }
        }

        //on cree la liaison : un anneau dont on retire les 3/4 :
        difference(){
            link(0,0,0);
            union(){
                translate([-linkOutRad/2-(rOut-rInn),0,0])cube([linkOutRad,10*linkOutRad,20*thick],             center=true);
                translate([linkOutRad/2,linkOutRad/2+(rOut-rInn),0])cube([linkOutRad+3*(rOut-rInn),linkOutRad,10*thick],             center=true);
            }
        }
        }
}

//deplacer la piece pour que la tige soit centree en l'origine
assembly();
```

![](../images/cantlever.PNG)

![](../images/cantlever_print.jpg)

Comme cette pièce a été faites par une autre personne,je laisse l'en-tête comme elle est.

La pièce ne se comportait pas comme je l'imaginais.

Donc,à la place,j'ai décidé de créer 2 beam avec mon code flexlinks mais après réflexion, il serait plus simple de faire un unique flexlink.

```
/*
    FILE    : flexlink_dual.scad

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-29

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

*/

$fn=100;

width=15.8;//mm the 2 extremities
depth=7;
height=3.2;
length_br=25;//mm la branche reliant les 2 extrémités

epaisseur=1;

hole_r=2.5;
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous

difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);

}



translate([width,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([width+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};

translate([(width*2)+length_br,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([(width*2)+length_br+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);


    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};

```
![](../images/flexlink_dual.PNG)

![](../images/dual_print.jpg)

Voici le resultat:


<video width="400" height="400" controls muted>
<source src="../../images/stables_red.mp4" type="video/mp4">
</video>

cliquez sur ce lien si la video marche pas:[video](../images/stables_red.mp4)

![video](../images/stables_red.mp4)



* Voici un lien pour mes codes source: [3D_print_files](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/tree/main/docs/3D_print_files)
