# 5. Electronique 1 - Prototypage

## Microcontroller : Arduino

Pour la première partie du module Electronique, on nous a enseigné comment utiliser un microcontroller, plus précisement un arduino. L'avantage de l'arduino est que tout est déja intégré dans son IDE et il est compatible avec plusieurs sensors qu'un assistant a listé dans ce [lien](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/07e9c48531529f84506c966be76957e4dba6ec1c/IO-Modules.md).

Mais utilisez l'arduino n'est pas sans danger, il faut respectez certaines spécificités comme le courant maximum par pin qui est de 40mA pour éviter d'endommager le microcontroller et [bien d'autres](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/raw/master/img/Arduino-uno-pinout.png)

## Utilisation

Pour utiliser l'Arduino, il nous faut un cable USB et télécharger le logiciel arduino IDE (integrated development environment) que vous pouvez trouvez dans ce [lien](https://www.arduino.cc/en/software).

Le logiciel Arduino IDE permet d'envoyer du code en langage c qui contiennent les taches demandés. Le logiciel possède beaucoup de fonctions comme:

* Exemples: Donne des exemples de code pour différentes fonctions.

* Gérer les bibliothèques: permet de télécharger les bibliothèques nécessaires en entrant leurs informations.

* Types de carte: permet de se connecter à n'importe quelle microcontrolleur dans la liste.

Après avoir créer le code, il nous suffit ensuite de cabler les différents composants avec l'arduino ou à l'aide d'un breadboard. Un breadboard est une planche troué permettant des connexions horizontales et verticales.

![](../images/breadboard.jpg)

![](../images/breadboard_conx.png)

## Taches

Pour cette partie, on nous a demandé de faire quelques exercices d'exemple venant de ce [lien](https://docs.arduino.cc/built-in-examples/).

Si les vidéos ne marchent pas, cliquez sur les liens au-dessus des videos.

### blink

J'ai commencé par monter les différents composant pour le blink code qui consiste à faire clignoter la led intégré et/ou une led connecter sur un breadboard en utilisant le code dans [les exemples](https://docs.arduino.cc/built-in-examples/basics/Blink) . Attention, il est déconseillé de connecter les cables et les composant sous alimentation.

[blink](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/blob/main/docs/images/blink.mp4)

![](../images/blink_reduced.mp4)

<video width="400" height="400" controls muted>
<source src="../../images/blink.mp4" type="video/mp4">
</video>


### analog Read Serial

L'arduino est connecter à un potentiomètre et [lit la valeur de la résistance](https://docs.arduino.cc/built-in-examples/basics/AnalogReadSerial)

[Analog Read Serial](../images/potentiometer_reduced.mp4)

![Analog Read Serial](../images/potentiometer_reduced.mp4)

<video width="400" height="400" controls muted>
<source src="../../images/potentiometer_reduced.mp4" type="video/mp4">
</video>

### digital Read Serial

Pour cette exercice, un interrupteur est connecter à l'arduino. Le [code](https://docs.arduino.cc/built-in-examples/basics/DigitalReadSerial) utilisé permet de savoir quand l'interrupteur est presser étant donné que l'interrupteur est un point reliant 2 point d'un cable. Quand il est fermé, 5V est envoyé à l'arduino qui considère le circuit fermé.

[Digital Read Serial](../images/push_button_reduced.mp4)

![Digital Read Serial](../images/push_button_reduced.mp4)



<video width="400" height="400" controls muted>
<source src="../../images/push_button_reduced.mp4" type="video/mp4">
</video>

### capteur de temperature

Il est nous est aussi demandé d'utiliser un capteur et de pouvoir lire les données. Pour lire les données, je me suis inspiré  du même [code](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/tree/main/docs/Arduino) pour le potentiomètre. Le capteur que j'ai utilisé est un capteur de température VMA320 avec les spécifications suivantes:

* NTC type: NTC-MF52 3950

* temperature range: -55 °C to 125 °C

* accuracy: +/- 0.5°C

* pull-up resistor: provided, 10 KOhm

* connection: 3 pin, (+) 5V , (-) ground, (S) analogue output

* dimensions: 20 x 15 x 5 mm

[temperature](../images/temperature_reduced.mp4)

![temperature](../images/temperature_reduced.mp4)


<video width="400" height="400" controls muted>
<source src="../../images/temperature_reduced.mp4" type="video/mp4">
</video>

D'après le [datasheet](https://www.velleman.eu/downloads/29/vma320_a4v01.pdf) du capteur de température VMA320, c'est un thermistor dont la resistance est linéairement proportionnelle à la température. La temperature ambiante est +/- 19°C et quand j'appuie dessus, la temperature monte à 22 °C.

## Méthodologie

Avant de programmer un code Arduino, il est important de regarder les datasheets des différents composants utilisés pour le montage. Les datasheets contiennent d'importante informations comme les précautions, des exemples de connexions, l'alimentation requis et limite,... . Ensuite, j'écris le code pour controler mon montage avec toutes les informations nécessaires.

## Mon code arduino

J'ai pris le temps de faire mon propre code arduino inspiré des exemples.

Ma liste de matériel utilisée:

* 3 leds de couleurs différentes (rouge, vert et jaune)

* 3 resistances

* 1 arduino uno

* 1 interrupteur

* des câbles

Mon code allume une led rouge montrant que le système a reçu mon code, fais clignoter une led jaune toutes les secondes et allume une led verte quand on appuie sur l'interrupteur.

```
/*
    FILE    : control_color.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-26

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : Blink of  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink

*/

int yellow=2;
int green=4;
int red = 3;
int i=0;
int pushButton = 12;

void setup() {
  // put your setup code here, to run once:
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(red, HIGH);
  if (i==0){
    digitalWrite(yellow, HIGH);
    i=1;
  }
  else {
    digitalWrite(yellow, LOW);
    i=0;
  }

  int buttonState = digitalRead(pushButton);

  if (buttonState==1){
    digitalWrite(green, HIGH);
  }
  else {
    digitalWrite(green, LOW);
  }

  delay(1000);   

}
```
[lien du code source](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/tree/main/docs/Arduino)

![](../images/board.jpg)

[](../images/board_video_red.mp4)

![](../images/board_video_red.mp4)

<video width="400" height="400" controls muted>
<source src="../../images/board_video_red.mp4" type="video/mp4">
</video>
