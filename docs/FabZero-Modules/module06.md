# 6. Electronique 2 - Fabrication

Pour la semaine suivant la première partie d'électronique, on a été chargé de construire notre propre microcontrolleur  

## Composants

Pour créer notre microcontrolleur, nous devions incorporer un microprocesseur 32 bits [Atmel SAMD11C](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42363-SAM-D11_Datasheet.pdf) avec 16 kB de mémoire.
Ce microprocesseur intègre nativement le protocole de communication USB avec ses bibliothèques laissant peu de place pour écrire un code. L'alimentation maximale qu'il peut supporter est de 3.3V donc pour convertir un voltage de 5V en 3.3V, un régulateur a été placé sur le PCB connecté avec une capacité sinon ça ne marche pas.

Les autres composants sont:

* 3 resistances (2 resistance de 330 ohms et une resistance de 100.1 ohms)

* 2 Leds (une led rouge et une led verte)

* 2 3.3V capacités

* 2 connecteurs

## Soudure

Pour placer les différents composants, nous les avons soudés avec le matériel de soudure du fablab sur un PCB industriel dont les zones de soudure sont affichés permettant de bien placés le microprocesseur et le régulateur. nous avons donc suivi le [tutoriel](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md) du site fablab.

![](../images/soudure.jpg)

![](../images/1_design.jpg)

Comparés à ce qui est écrit dans le site, le bootloader n'estpas intégré dans le Atmel SAMD11C donc il était important de souder les connecteurs pour uploader le bootloader. La soudure des connecteurs a été quelque peu difficile car la zone correspond au ground dissipe facilement la chaleur ce qui a demandé beaucoup d'application et l'aide du superviseur.

![](../images/2_design.jpg)

Après avoir uploader le bootloader, j'ai connecter le reste des composants.

![](../images/final_design.jpg)

 En connectant mon microcontrolleur à mon pc, la led d'alimentation s'allume montrant que mon microcontrolleur se connecte à mon PC.

![](../images/led_on.jpg)

Mais malheureusement mon pc ne reconnait pas la connexion usb m'empêchant de lancer un code. Ce problème apparait à d'autre camarade de ce cours. Un des superviseurs a regardé mon travail et a vu que le problème ne vient pas de ma soudure et j'ai comparé avec le travail des autres mais ça n'a donné aucune solution. Le problème doit probablement venir du PCB industrielles.

## Code de controle de la led.

Etant donné que mon PCB ne marche pas, j'ai testé avec le microcontrolleur d'une camarade Nathalie WÉRON. Pour l'utiliser,j'ai ajouté le fichier `https://www.mattairtech.com/software/arduino/package_MattairTech_index.json` a l'external board manager et télécharger la bibliothèque. Ensuite, j'ai sélectionner ces options dans l'onglet outils de mon Arduino IDE:

* `Board` : Generic D11C14A

* `Clock` : INTERNAL_USB_CALIBRATED_OSCILLATOR

* `USB-CONFIG` : CDC_ONLY

* `SERIAL_CONFIG` : ONE_UART_ONE_WIRE_NO_SPI

* `BOOTLOADER` : 4kB Bootloader

Après avoir mis la carte dans le port usb de mon pc, j'ai envoyer ce code:

```
/*
  FILE    : Blink_atmel.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-31

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Adapted from basics examples of the arduino ide.

*/
int led=15;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);                       // wait for a second
}

```   

La led verte clignote rapidement.


![](../images/led_blink_red.mp4)

<video width="400" height="400" controls muted>
<source src="../../images/led_blink_red.mp4" type="video/mp4">
</video>

cliquer sur ce [lien](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/blob/main/docs/images/led_blink_red.mp4) si la video ne marche pas.
