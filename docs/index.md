## A propos de moi

Bonjour, Je m'appelle Ghani Sajl (qu'on prononce Sajil mais on l'écrit sans le i).

Je suis étudiant polytech de l'ULB en dernière année de master en Electromecanique,option robotique et construction mécanique. Dans ma page fablab, vous aurez une description des 6 modules présentés du cours de Fabzero experiment et de la progression de mon projet.

J'ai choisi de suivre ce cours car je suis intéresser par les techniques de fabrication numérique et je voudrais approfondir mes compétences et acquérir plus d'expérience.



## Centre d'intérêt et loisirs

Je suis intéresser par les nouvelles technologies et l'informatique donc je vise un emploi qui allie informatique et électronique. J'aime la lecture et les jeux vidéo.

## Mes projets réalisés

* Création d'un jeu de poker en python


* Création d'un prototype de jeu de sim en JAVA


* Création du back-end d'un jeu de réhabilitation


* Création d'un tapis de roulement
