$fn=100;

width=15.8;//mm the 2 extremities
depth=7;
height=3.2;
length_br=25;//mm la branche reliant les 2 extrémités

epaisseur=1;

hole_r=2.5;
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous

difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);
    
    
    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r);
     
    translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);
    
    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);

}



translate([width,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([width+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);
    
    
    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r); 

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);
    
    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};

translate([(width*2)+length_br,depth/2,0])cube([length_br, epaisseur/2,height]);

translate([(width*2)+length_br+length_br,0,0])difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);
    
    
    translate([width,depth/2,0])cylinder(h=height,r=depth/2);
}
    translate([width/4,depth/2,0])cylinder(h=height*2,r=hole_r); 

translate([width/4,depth/2,eps])cylinder(h=height*2,r=hole_r);
    
    translate([(width/4+dist_hole),depth/2,0])cylinder(h=height*2,r=hole_r);

    translate([(width/4+dist_hole),depth/2,eps])cylinder(h=height*2,r=hole_r);
};
