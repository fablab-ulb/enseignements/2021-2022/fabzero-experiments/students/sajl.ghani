$fn=100;

width=15.8;//mm the 2 extremities
depth=7;
height=3.2;

hole_r=2.6;
dist_hole=8;//distance entre les 2 centres des trous

eps=-5;// pour couper en dessous

difference(){
    hull(){
    translate([0,depth/2,0])cylinder(h=height,r=depth/2);
    cube([width, depth,height]);
    
    
    translate([width+40,depth/2,0])cylinder(h=height,r=depth/2);
}
    
for (i = [0:6]) { 
    
    translate([(width/4+(dist_hole*i)),depth/2,0])cylinder(h=height*2,r=hole_r-(0.1*i));

    translate([(width/4+(dist_hole*i)),depth/2,eps])cylinder(h=height*2,r=hole_r-(0.1*i));
    
}

}
