/*
    FILE    : cantileverBeam.scad
    
    AUTHOR  : Paul Bryssinck <paulbryssinck@hotmail.com>
    
    DATE    : 2021-02-27
    
    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/
    
    Original design by the BYU Compliant Mechanisms Research Group (CMR) (https://www.compliantmechanisms.byu.edu/flexlinks)
*/

//parametres
$fn=100;
pi=3.1415926535;
thick=3; //epaisseur de la piece (direction z)
//fixation:
nHoles=2;
rOut=4; //largeur fixation (direction y) (la longueur de la fixation est nHoles*2*rOut)
rInn=2.5; //taille des trous
holeSep=0;//pour controler l'espacement entre les trous
//liaison
linkLen=20; //longueur de la tige depliee
linkWid=1.5;
r=2*linkLen/pi;
linkOutRad=r+linkWid/2;
linkInnRad=r-linkWid/2;

//coeur de la fixation :
module filledClip(xPos,yPos,zPos){
    hull(){
        cylinder(h=thick, r=rOut);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=rOut);
        }
}

//liaison :
module link(xPos,yPos,zPos){
    difference(){
        translate([xPos,yPos,zPos])cylinder(h=thick, r=linkOutRad);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=linkInnRad);
        }
}

//Assemblage et perçage des différentes parties
module assembly(){
    union(){
        //fixation 1 (touchant l'axe y)
        translate([-rOut,-r,0])rotate([0,0,180])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick+10,r=rInn);
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick-50,r=rInn);
            }
        }
        //fixation 2 (touchant l'axe x)
        translate([+r,rOut,0])rotate([0,0,90])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick+10,r=rInn);
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick-50,r=rInn);
            }
        }
        
        //on cree la liaison : un anneau dont on retire les 3/4 :
        difference(){ 
            link(0,0,0);
            union(){
                translate([-linkOutRad/2-(rOut-rInn),0,0])cube([linkOutRad,10*linkOutRad,20*thick],             center=true);
                translate([linkOutRad/2,linkOutRad/2+(rOut-rInn),0])cube([linkOutRad+3*(rOut-rInn),linkOutRad,10*thick],             center=true);
            }
        }
        }
}

//deplacer la piece pour que la tige soit centree en l'origine
assembly();





