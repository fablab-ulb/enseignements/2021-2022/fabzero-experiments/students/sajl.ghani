/*
    FILE    : proto_code.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com> and Alessandro Abbracciante <alessandro.abbracciante@ulb.be>

    DATE    : 2022-05-12

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : minimal MPU-6050 tilt and roll (sjr) from https://forum.arduino.cc/t/mpu6050-unstable-values-drift/624303/2

    and NeoPixel Ring simple sketch from  Adafruit NeoPixel library

*/


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#include<Wire.h>
const int MPU_addr1 = 0x68;
float xa, ya, za, roll, pitch, pre_pitch;
unsigned long start;
float limit_right;
float limit_left;

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 3 // Popular NeoPixel ring size

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN_red        6 // On Trinket or Gemma, suggest changing this to 1
#define PIN_green        5 // On Trinket or Gemma, suggest changing this to 1

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_red, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN_green, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 500 // Time (in milliseconds) to pause between pixels


void setup() {
  // put your setup code here, to run once:

  Wire.begin();                                      //begin the wire communication
  Wire.beginTransmission(MPU_addr1);                 //begin, send the slave adress (in this case 68)
  Wire.write(0x6B);                                  //make the reset (place a 0 into the 6B register)
  Wire.write(0);
  Wire.endTransmission(true);                        //end the transmission
  Serial.begin(9600);

  pre_pitch=0.0;
  
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

}

void loop() {
  // put your main code here, to run repeatedly:
  pixels.setPixelColor(0, pixels.Color(0, 0, 0));
  pixels.setPixelColor(1, pixels.Color(0, 0, 0));
  pixels.setPixelColor(2, pixels.Color(0, 0, 0));
  pixels.show();   // Send the updated pixel colors to the hardware.

  strip.setPixelColor(0, pixels.Color(0, 0, 0));
  strip.setPixelColor(1, pixels.Color(0, 0, 0));
  strip.setPixelColor(2, pixels.Color(0, 0, 0));
  strip.show();   // Send the updated pixel colors to the hardware.

  Wire.beginTransmission(MPU_addr1);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr1, 6, true); //get six bytes accelerometer data

  xa = Wire.read() << 8 | Wire.read();
  ya = Wire.read() << 8 | Wire.read();
  za = Wire.read() << 8 | Wire.read();

  roll = atan2(ya , za) * 180.0 / PI;
  pitch = atan2(-xa , sqrt(ya * ya + za * za)) * 180.0 / PI;

  Serial.print("roll = ");
  Serial.print(roll,1);
  Serial.print(", pitch = ");
  Serial.println(pitch,1);

  limit_right=40;

  stop_clignotant=10000; //10 seconds
  
  if (pitch-pre_pitch >limit_right){
      start = millis();
      while (millis() - start <stop_clignotant){
        Serial.println(millis() - start);
        pixels.setPixelColor(0, pixels.Color(250, 0, 0));
        //pixels.show();   // Send the updated pixel colors to the hardware.
        pixels.setPixelColor(1, pixels.Color(250, 0, 0));
        //pixels.show();   // Send the updated pixel colors to the hardware.
        pixels.setPixelColor(2, pixels.Color(250, 0, 0));
        pixels.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL); // Pause before next pass through loop
        pixels.setPixelColor(0, pixels.Color(0, 0, 0));
        pixels.setPixelColor(1, pixels.Color(0, 0, 0));
        pixels.setPixelColor(2, pixels.Color(0, 0, 0));
        pixels.show();   // Send the updated pixel colors to the hardware.
        delay(DELAYVAL); // Pause before next pass through loop
      }
  }

  limit_left=40;

  if (pitch-pre_pitch <-limit_left){
      start = millis();
      while (millis() - start <stop_clignotant){
        Serial.println(millis() - start);
        strip.setPixelColor(0, pixels.Color(0, 250, 0));
        strip.setPixelColor(1, pixels.Color(0, 250, 0));
        strip.setPixelColor(2, pixels.Color(0, 250, 0));
        strip.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL); // Pause before next pass through loop
        strip.setPixelColor(0, pixels.Color(0, 0, 0));
        strip.setPixelColor(1, pixels.Color(0, 0, 0));
        strip.setPixelColor(2, pixels.Color(0, 0, 0));
        strip.show();   // Send the updated pixel colors to the hardware.
        delay(DELAYVAL); // Pause before next pass through loop
      }
  }

  delay(400);

}
