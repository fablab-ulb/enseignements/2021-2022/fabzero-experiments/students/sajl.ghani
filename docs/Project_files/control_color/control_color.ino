/*
    FILE    : control_color.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com>

    DATE    : 2022-03-26

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : Blink of  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink

*/

int yellow=2;
int green=4;
int red = 3;
int i=0;
int pushButton = 12;

void setup() {
  // put your setup code here, to run once:
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(red, HIGH);
  if (i==0){
    digitalWrite(yellow, HIGH);
    i=1;
  }
  else {
    digitalWrite(yellow, LOW);
    i=0;
  }

  int buttonState = digitalRead(pushButton);

  if (buttonState==1){
    digitalWrite(green, HIGH);
  }
  else {
    digitalWrite(green, LOW);
  }
  
  delay(1000);   

}
